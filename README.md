# srget
Data Communication Networks - Project 1: Resumable Concurrent File Download

* Single thread standard download is reliable and fast
* Downloads are resumable
* Concurrent Download works and is fast but sometimes fails as the DLCONTENT of the parts are not completely written to the main DLCONTENT file
* Doesn't yet support chunked transfer encoding