#! /usr/bin/env python
# -*- coding: utf-8 -*-

import asyncore
import logging
import os
import io
import shutil
import socket
import sys
import getopt
import time
from cStringIO import StringIO
from urlparse import urlparse
import threading
from Queue import Queue

downloadobj = Queue()
processQueu = Queue()
amap = {}
filelist = []


def usage():
    print "usage: srget -o O [-c[numconn]] URL"


def make_request(req_type, what, details, ver="1.1"):
    """ Compose an HTTP request """
    NL = "\r\n"
    req_line = "{verb} {w} HTTP/{v}".format(
        verb=req_type, w=what, v=ver
    )
    details = [
        "{name}: {v}".format(name=n, v=v) for (n, v) in details.iteritems()
    ]
    detail_lines = NL.join(details)
    full_request = "".join([req_line, NL, detail_lines, NL, NL])
    return full_request


def parse_url(url, DEFAULT_PORT=80):
    """ Parse a given url into host, path, and port.
       Use DEFAULT_PORT (80) if unspecified.
   """
    parsed_url = urlparse(url)
    scheme, host, path, port = (parsed_url.scheme,
                                parsed_url.hostname,
                                parsed_url.path,
                                parsed_url.port)
    if not port:
        port = DEFAULT_PORT
    if not path:
        path = '/'
    # host = socket.gethostbyname(host)
    return (scheme, host, path, port)


def extractHeader(head):
    metainf = dict()
    split = head.split('\r\n\r\n')
    if len(split) != 2:
        return ({}, None)
    content = split[1]
    head = split[0]
    bytes_head = len(head) + 4

    headLines = head.split('\r\n')
    metainf['Status'] = headLines[0]
    metainf['bytes_head'] = bytes_head
    for lines in headLines[1:]:
        field, value = lines.split(': ')
        metainf[field] = value
    return (metainf, content)


def sendPacket(socket, msg):
    totalsent = 0
    peices = 8000
    while totalsent < len(msg):
        sent = socket.send(msg[totalsent:totalsent + peices])
        if sent == 0:
            raise RuntimeError("socket connection broken")
        totalsent = totalsent + sent


def getStatus(status):
    return int(status[9])


def receivePacket(socket):
    bytes_recd = 0
    packet = StringIO()
    while True:
        try:
            chunk = socket.recv(4096)
        except socket.timeout:
            break
        if chunk == '':
            break
        bytes_recd += len(chunk)
        # print str(len(chunk))
        packet.write(chunk)
    return packet.getvalue()


def getHeadMetainf(host, port):
    """ Returns a dictionary of the URL attained from a HEAD Request """
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.settimeout(5)
    try:
        s.connect((host, port))
    except socket.timeout:
        print 'Unable to establish connection'
        sys.exit(2)
    request = make_request(
        'HEAD', path, {
            'Host': host + ':' + str(port),
            'Connection': 'close'}
    )
    sendPacket(s, request)
    response = receivePacket(s)
    metainf, content = extractHeader(response)
    logging.debug('Got head dictionary')
    return metainf


def concurrentHandler(metainf, url, filename, concurrentC=5, startpoint=0):
    start = time.time()
    try:
        contentLength = int(metainf['Content-Length'])
        if 'bytes' not in metainf['Accept-Ranges']:
            print 'Does not support concurrent download'
            return None
    except KeyError:
        print 'Does not support concurrent download'
        return None
    HFileP = open('.'+filename+'.DLHEAD', 'w+')
    HFileP.seek(0, 0)
    HFileP.write('{}\r\n'.format(
        metainf.get('ETag', 'NULL')))
    HFileP.write('{}\r\n'.format(
        metainf.get('Content-Length', 'NULL')))
    HFileP.write('{}\r\n'.format(
        metainf.get('Last-Modified', 'NULL')))
    HFileP.write('{}\n'.format(0))
    if contentLength/4 > 52428800:
        chunkSize = 52428800  # 50 megabytes
    elif contentLength/4 > 10485760:
        chunkSize = 10485760  # 10 megabytes
    elif contentLength/4 > 5242880:
        chunkSize = 5242880  # 5 megabytes
    else:
        chunkSize = 4194304  # 1 megabyte
    start = startpoint/chunkSize
    completeChunks = (contentLength/chunkSize)
    if (startpoint) % chunkSize != 0:
        print start % chunkSize
        ConcurrentDownloader(
            metainf,
            filename,
            concurrentC,
            url,
            startpoint,
            (start+1)*chunkSize,
            num=start)
        start += 1
    for i in range(start, completeChunks):
        ConcurrentDownloader(
            metainf,
            filename,
            concurrentC,
            url,
            i*chunkSize,
            (i+1)*chunkSize,
            num=i)
    ConcurrentDownloader(
        metainf,
        filename,
        concurrentC,
        url,
        completeChunks*chunkSize,
        contentLength,
        end=True,
        num=completeChunks)
    while True:
        if len(amap) < concurrentC:
            dl = downloadobj.get()
            dl.updateMap()
            continue
        break
    try:
        thread = threading.Thread(target=runLoop)
        thread.start()
        topdownlobj = processQueu.get()
        while processQueu.qsize() > 0:
            if downloadobj.qsize() > 0 and len(amap) < concurrentC:
                dl = downloadobj.get()
                dl.updateMap()
                continue
            try:
                sock = topdownlobj.getsocket()
            except socket.error:
                sock = -1
            if sock not in amap:
                with open('.'+filename+'.DLCONTENT', 'a+b') as wfd:
                    with open(
                              '.'+topdownlobj.getfile() +
                              '.DLCONTENT', 'rb') as fd:
                        shutil.copyfileobj(fd, wfd, 1024*1024*10)
                    os.remove('.'+topdownlobj.getfile()+'.DLCONTENT')
                    os.remove('.'+topdownlobj.getfile()+'.DLHEAD')
                HFileP.write('{}\n'.format(os.path.getsize(
                    '.'+filename+'.DLCONTENT')))

                percent = "{0:.2f}".format(
                    (os.path.getsize('.'+filename+'.DLCONTENT') /
                     float(contentLength)) * 100.0)
                fill = int(round(50 *
                           (os.path.getsize('.'+filename+'.DLCONTENT') /
                            float(contentLength))))
                bar = '█' * fill + '-' * (50 - fill)
                sys.stdout.write('\rProgress |%s| - %s%s ' % (bar,
                                                              percent, '%'))
                sys.stdout.flush()

                topdownlobj = processQueu.get()
        # Write Last top download object
        with open('.'+filename+'.DLCONTENT', 'a+b') as wfd:
            with open('.'+topdownlobj.getfile()+'.DLCONTENT', 'rb') as fd:
                shutil.copyfileobj(fd, wfd, 1024*1024*10)
            os.remove('.'+topdownlobj.getfile()+'.DLCONTENT')
            os.remove('.'+topdownlobj.getfile()+'.DLHEAD')
        HFileP.write('{}\n'.format(os.path.getsize(
                '.'+filename+'.DLCONTENT')))
        percent = "{0:.2f}".format(
            (os.path.getsize('.'+filename+'.DLCONTENT') /
             float(contentLength)) * 100.0)
        fill = int(round(50 * (os.path.getsize('.'+filename+'.DLCONTENT') /
                               float(contentLength))))
        bar = '█' * fill + '-' * (50 - fill)
        sys.stdout.write('\rProgress |%s| - %s%s ' % (bar, percent, '%'))
        sys.stdout.flush()
        if os.path.getsize('.'+filename+'.DLCONTENT') != contentLength:
            print 'Something went wrong'
            sys.exit(2)
        print '\nbegin write to file'
        with open(filename, 'a+b') as wfd:
            with open('.'+filename+'.'+'DLCONTENT', 'rb') as fd:
                shutil.copyfileobj(fd, wfd, 1024*1024*10)
            os.remove('.'+filename+'.'+'DLCONTENT')
            os.remove('.'+filename+'.DLHEAD')
        print 'done'
        sys.exit(0)
    except (KeyboardInterrupt):
        print '\nInterrupted Threading'
    finally:
        HFileP.close()
        for f in filelist:
            if os.path.isfile(f+'.DLCONTENT'):
                pass
                os.remove(f+'.DLCONTENT')
            if os.path.isfile(f+'.DLHEAD'):
                os.remove(f+'.DLHEAD')
        sys.exit(0)


def ConcurrentDownloader(
     metainf, filename, concurrentC, url, rangestart, rangeend,
     end=False, num=0):
    if rangestart == rangeend:
        return
    fileLen = int(metainf['Content-Length'])
    global downloadobj
    global processQueu
    files = []
    differ = rangeend - rangestart
    curren = rangestart
    size = differ/concurrentC
    for i in range(concurrentC-1):
        downloader = HTTPDownloader(
            url,
            filename+'.'+str(num)+'.'+str(i),
            realFileLen=fileLen,
            range=curren,
            concurrent=True,
            endRange=curren+size-1)
        downloadobj.put(downloader)
        processQueu.put(downloader)

        curren += size
        files += ['.'+filename+'.'+str(num)+'.'+str(i)]
    if end:
        downloader = HTTPDownloader(
            url,
            filename+'.'+str(num)+'.'+str(concurrentC-1),
            realFileLen=fileLen,
            range=curren,
            concurrent=True,
            endRange=rangeend)
        downloadobj.put(downloader)
        processQueu.put(downloader)
    else:
        downloader = HTTPDownloader(
            url,
            filename+'.'+str(num)+'.'+str(concurrentC-1),
            realFileLen=fileLen,
            range=curren,
            concurrent=True,
            endRange=rangeend-1)
        downloadobj.put(downloader)
        processQueu.put(downloader)
    # print amap
    files += ['.'+filename+'.'+str(num)+'.'+str(concurrentC-1)]
    global filelist
    filelist += files


def runLoop():
    try:
        asyncore.loop(use_poll=True, map=amap)
    except (KeyboardInterrupt, SystemExit):
        print '\nInterrupted'
        for f in filelist:
            if os.path.isfile(f):
                os.remove(f)
            sys.exit(0)
    sys.exit(0)


class HTTPDownloader(asyncore.dispatcher):
    RECV_CHUNK_SIZE = 8192

    def __init__(self, url, o, resume=False, range=0, realFileLen=0,
                 concurrent=False, endRange=0):
        asyncore.dispatcher.__init__(self)
        scheme, host, path, port = parse_url(url)
        # print host, path, port

        if scheme != "http":
            print "Must use HTTP"
            sys.exit(1)
        self.resume = resume
        self.concurrent = concurrent
        self.fileName = o
        self.range = range
        self.endRange = endRange
        self.logger = logging.getLogger(url)

        self.create_socket(socket.AF_INET, socket.SOCK_STREAM)
        self.logger.debug("Created Socket")
        self.connect((host, port))

        self.real_file_len = realFileLen
        # print self.real_file_len
        self.bytes_recv = range
        self.bytes_head = 0

        self.host = host
        self.metainf = dict()
        self.gotHead = False
        self.packetHead = StringIO()

        self.fileName = o
        self.ContentFilename = "." + o + ".DLCONTENT"
        self.HeadFilename = "." + o + ".DLHEAD"

        (self.recvbuf, self.sendbuf) = ("", "")

        if resume:
            print "Have {0:.2f}%".format(
                (self.range / float(self.real_file_len)) * 100.0)
            print 'Starting Resume'

        if self.concurrent:
            request = make_request(
                'GET', path, {
                    'Host': host + ':' + str(port),
                    'Connection': 'close',
                    'Range':
                        ('bytes=' + str(self.range) + '-' +
                         str(self.endRange))}
            )
        else:
            request = make_request(
                'GET', path, {
                    'Host': host + ':' + str(port),
                    'Connection': 'close',
                    'Range': 'bytes=' + str(self.range) + '-'}
            )
        self.write(request)
        self.CFileP = open(self.ContentFilename, 'ab')
        self.HFileP = open(self.HeadFilename, 'a+')

    def updateMap(self):
        global amap
        amap.update({self.socket.fileno(): self})

    def getfile(self):
        return self.fileName

    def getsize(self):
        return self.endRange

    def getsocket(self):
        return self.socket.fileno()

    def write(self, data):
        self.sendbuf += data

    def handle_connect(self):
        self.logger.debug("Connected")

    def handle_close(self):
        self.logger.debug("Disconnected")
        self.CFileP.close()
        if self.concurrent:
            global amap
            del amap[self.socket.fileno()]
        if (((self.bytes_recv) / float(self.real_file_len)) == 1 and
                os.path.isfile(self.ContentFilename) and not self.concurrent):
            os.rename(self.ContentFilename, self.fileName)
            os.remove(self.HeadFilename)
            print '\n- Complete -'
        self.close()

    def handle_error(self):
        self.logger.error("Some Error")
        print '\nFatal error'
        sys.exit(0)

    def handle_expt(self):
        self.logger.critical("Some Exception")

    def writable(self):
        return len(self.sendbuf) > 0

    def handle_write(self):
        bytes_sent = self.send(self.sendbuf)
        self.sendbuf = self.sendbuf[bytes_sent:]

    def handle_read(self):
        recv = self.recv(HTTPDownloader.RECV_CHUNK_SIZE)
        if not recv:
            self.logger.info('recvd none')
            return
        bytes_recv = len(recv)
        # self.logger.debug("recvd {} bytes".format(bytes_recv))
        if not self.gotHead:
            self.packetHead.write(recv)
            (
                metainf,
                content
            ) = extractHeader(self.packetHead.getvalue())
            if metainf is not None:
                self.metainf = metainf
                self.bytes_head = self.metainf['bytes_head']
                if 'Content-Length' in self.metainf:
                    self.bytes_content = self.metainf['Content-Length']
                self.gotHead = True
                self.CFileP.write(content)
                self.bytes_recv += len(content)
                self.HFileP.seek(0, 0)
                self.HFileP.write('{}\r\n'.format(
                    self.metainf.get('ETag', 'NULL')))
                self.HFileP.write('{}\r\n'.format(
                    self.metainf.get('Content-Length', 'NULL')))
                self.HFileP.write('{}\r\n'.format(
                    self.metainf.get('Last-Modified', 'NULL')))
                self.HFileP.write('{}\r\n'.format(
                    self.metainf.get('bytes_head', 'NULL')))
                self.HFileP.write('{}\n'.format(self.bytes_recv))
                getStatus(self.metainf['Status'])
                if self.real_file_len == 0:
                    self.real_file_len = int(self.metainf['Content-Length'])
        else:
            self.bytes_recv += bytes_recv
            self.CFileP.write(recv)
            self.HFileP.write('{}\n'.format(self.bytes_recv))

            percent = "{0:.2f}".format(
                (self.bytes_recv / float(self.real_file_len)) * 100.0)
            fill = int(
                round(50 * (self.bytes_recv / float(self.real_file_len))))
            if not self.concurrent:
                bar = '█' * fill + '-' * (50 - fill)
                sys.stdout.write('\rProgress |%s| - %s%s ' %
                                 (bar, percent, '%'))
                sys.stdout.flush()

# NOTE: main
if __name__ == "__main__":
    # Setup Logging
    logging.basicConfig(
        filename='testlog.log',
        level=logging.DEBUG,
        format="%(asctime)-15s [%(levelname)s] %(name)s: %(message)s"
    )

    logging.info('Start srget')

    # Parse Arguments
    filename = None
    concurrent = False
    concurrentC = 5
    url = None
    optlist, args = getopt.getopt(sys.argv[1:], 'o:c')
    for opt, arg in optlist:
        if opt in ('-o'):
            filename = arg
        elif opt in ('-c'):
            concurrent = True
            if len(args) == 2 and args[0].isdigit():
                concurrentC = int(args[0])
                url = args[1]
            elif len(args) == 1:
                url = args[0]
            else:
                usage()
                sys.exit()
        else:
            usage()
            sys.exit(2)
    if not concurrent and len(optlist) == 1 and len(args) == 1:
        url = args[0]
    elif concurrent:
        pass
    else:
        usage()
        sys.exit(2)

    # Send HEAD request and create dictionary and loop for redirect
    scheme, host, path, port = parse_url(url)
    while True:
        if scheme != "http":
            logging.error('HTTP not specified')
            print 'HTTP not specified'
            sys.exit(2)
        metainf = getHeadMetainf(host, port)
        statusline = metainf.get('Status', 'NULL')
        if 'HTTP/1.1' not in statusline and 'HTTP/1.0' not in statusline:
            logging.error('Server not in HTTP/1.1')
            print 'Server not responding in HTTP/1.1'
            sys.exit(2)

        statusClass = getStatus(metainf['Status'])
        print statusClass
        if statusClass == 4:
            logging.error('Client Error')
            print 'Client Error'
            sys.exit(0)
        elif statusClass == 3:
            logging.info('Redirect')
            if 'Location' in metainf:
                print 'redirect'
                link = metainf['Location']
                linkinf = urlparse(link)
                if linkinf[0] and linkinf[1]:
                    scheme, host, path, port = parse_url(link)
                else:
                    path = linkinf[2]
                continue
            else:
                pass
        elif statusClass == 2:
            logging.info('OK')
            print 'OK'
            break
        else:
            logging.error('Unhandled response')
            print 'Unhandled response'
            sys.exit(2)
        print 'Error'
        sys.exit(2)

    if (concurrent and concurrentC < 2) or not concurrent:
        print "Standard download"

    if (os.path.isfile("." + filename + ".DLHEAD") and
            os.path.isfile("." + filename + ".DLCONTENT")):
        meta = io.open("." + filename + ".DLHEAD", "r+b")
        # print meta.read()
        etag = meta.readline().strip()
        try:
            clength = int(meta.readline().strip())
        except ValueError:
            clength = None
        lmodified = meta.readline().strip()
        try:
            bytes_head = int(meta.readline().strip())
        except ValueError:
            bytes_head = None
        # print etag, clength, lmodified, bytes_head
        meta.seek(-2, 2)
        while meta.read(1) != b"\n":
            meta.seek(-2, 1)
        try:
            bytes_recv = int(meta.readline().strip())
        except ValueError:
            bytes_recv = None

        meta.close()
        if os.path.getsize("." + filename + ".DLCONTENT") == bytes_recv:
            scheme, host, path, port = parse_url(url)

            if scheme != "http":
                print "Must use HTTP"
                sys.exit(1)
            # metainf = getHeadMetainf(host, port)
            file_len = metainf['Content-Length']
            os.remove("." + filename + ".DLHEAD")
            if metainf['Accept-Ranges'] == 'bytes':
                if etag != 'NULL':
                    if metainf['ETag'] == etag:
                        if concurrent:
                            print 'resuming concurrent'
                            concurrentHandler(metainf, url, filename,
                                              concurrentC, bytes_recv)
                            sys.exit(0)
                        else:
                            print 'resuming standard'
                            HTTPDownloader(
                                url,
                                filename,
                                True,
                                bytes_recv,
                                file_len
                            )
                            try:
                                asyncore.loop()
                            except KeyboardInterrupt, SystemExit:
                                print '\nInterrupted Again'
                                sys.exit(2)
                    else:
                        print "Entity tag not matching, file changed"
                        sys.exit(1)
                elif ((lmodified == metainf['Last-Modified'])):
                    if concurrent:
                            print 'resuming concurrent'
                            concurrentHandler(metainf, url, filename,
                                              concurrentC, bytes_recv)
                            sys.exit(0)
                    else:
                        print 'resuming standard'
                        HTTPDownloader(url, filename, resume=True,
                                       range=bytes_recv, realFileLen=file_len)
                        try:
                            asyncore.loop()
                        except KeyboardInterrupt, SystemExit:
                            print '\nInterrupted Again'
                            sys.exit(2)
                else:
                    print "File possibly changed"
                    sys.exit(1)
            else:
                print "Server does not support partial download"
                sys.exit(0)
        else:
            print "File size not matching"
    elif concurrent:
        # print metainf
        concurrentHandler(metainf, url, filename, concurrentC)
        sys.exit(0)
    else:
        HTTPDownloader(url, filename)
        try:
            start = time.time()
            asyncore.loop(use_poll=True)
        except KeyboardInterrupt, SystemExit:
            print '\nInterrupted'
            sys.exit(2)
